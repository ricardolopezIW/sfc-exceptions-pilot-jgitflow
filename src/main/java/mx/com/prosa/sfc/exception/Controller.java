package mx.com.prosa.sfc.exception;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${message}")
    private String message;

    @GetMapping
    public String ok(){
        return ">" + message;
    }
}
