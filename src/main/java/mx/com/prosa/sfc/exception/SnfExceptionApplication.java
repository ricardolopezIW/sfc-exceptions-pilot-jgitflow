package mx.com.prosa.sfc.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@SpringBootApplication
public class SnfExceptionApplication {

	public static void main(String[] args) {
		System.out.println("##################################");
		System.out.println("Date: ");
		System.out.println(LocalDateTime.now().atZone(ZoneId.ofOffset("UTC", ZoneOffset.ofHours(-6))));
		SpringApplication.run(SnfExceptionApplication.class, args);
	}

}
