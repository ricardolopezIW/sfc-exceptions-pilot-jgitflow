## Excepciones de SFC

Este projecto contiene las clases de exepciones usadas en los aplicativos de SFC Prosa.

### Construccion

Ejecutar el siguiente comando para compilar el projecto.

`$ mvn clean install -DskipTests`

### Unit test

Con pruebas unitarias:

`$ mvn clean install`

### Despligue de componente a repositorio de snapshots de nexus.

`$ mvn clean deploy -Dmaven.test.skip=true -Dnexus.url.snapshots=http://157.245.95.203:8081 -Dnexus.server.id=nexus`

Donde :

 - nexus.url.snapshots : Ip y puerto del servidor de nexus. 
 - nexus.server.id: Identificador del servidor de nexus dentro del archivo en ${HOME}/.m2/settings.xml
 
 mvn jgitflow:release-start
 mvn jgitflow:release-finish