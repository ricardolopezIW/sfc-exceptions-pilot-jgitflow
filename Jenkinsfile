#!groovy?
pipeline {

    agent any
    tools {
        maven 'apache-maven-3'
    }

    environment {
        NEXUS_URL = 'http://157.245.95.203:8081'
        NEXUS_SERVER_ID = 'nexus'
    }

    stages {
        stage('Checkout') {
            steps {
                echo 'Checkout'
            }
        }

        stage('Compile') {
            steps {
                echo 'Clean Compile'
                sh 'mvn clean compile -U'
            }
        }

        stage('Package') {
            steps {
                echo 'Packaging'
                sh 'mvn package -DskipTests -U'
            }
        }

        stage('Publish') {
            steps {
                echo 'Publish artifact in Nexus repository'
                sh 'mvn --settings settings.xml clean deploy -Dmaven.test.skip=true -Dnexus.url.snapshots=' + NEXUS_URL + ' -Dnexus.server.id=' + NEXUS_SERVER_ID
            }
        }
    }

    post {
        success {
            echo 'JENKINS PIPELINE HAS BEEN EXECUTED SUCCESSFULLY'
            sendEmail("SUCCESS")
        }
        failure {
            echo 'JENKINS PIPELINE HAS BEEN FAILED'
            sendEmail("FAILED")
        }
    }

}

def sendEmail(String status){
    script {
        committerEmail = sh (
                script: 'git --no-pager show -s --format=\'%ae\'',
                returnStdout: true
        ).trim()
        mail    to: committerEmail,
                subject: "JENKINS Build: \"${env.JOB_NAME}\" - " + status,
                body: "Job "+ status +" - \"${env.JOB_NAME}\" build: ${env.BUILD_NUMBER}\n\nView the log at:\n ${env.BUILD_URL}"
    }
}